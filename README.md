# Design Driven Project - Final documentation

## Digital Documentation Application for Construction Site

### Yanran Chen, Kaiwen Ding, Ziyu Zhao

## **Introduction**
In order to reduce rework by strengthening communication, we build a platform to display images and 3d scanning models of construction site from time to time, so that everyone could check and discuss the building process remotely whenever it is.
 <br>

**Problem Statement** 

Rework is making changes of the already built things, then build it one more time. In the construction period, rework is an overlooked problem, but it cannot be ignored that rework brings unexpected losses. According to our research, the global construction industry spends approximately 178 billion dollars annually on rework activities
 <br>

**Vision** 

Generate VR scenes by scanning construction sites and photos taken directly by mobile devices by storing construction details to complete the documentation storage of the construction process. Partners from various fields can track the information of the construction site in real time from a remote location and give timely feedback to reduce rework caused by lack of communication.
 <br>

**Note**
to see our presentation slides please click here [![slides]](https://gitlab.com/rwth-crmasters-wise2122/ddp-topic-b/ddp-group5-project/-/tree/main/slides)  
 <br>

## **Project Files**
obj - 3D scanning models(.obis) created by RTAB-map<br>
vr - VR scences exported from unity<br>
slides - Our presentation slides
 <br>
 
## **Prototype Setup**
**Softwares Setup** 

- Node-RED <br>
Node-RED provides a browser-based flow editor that makes it easy to wire together flows using the wide range of nodes in the palette.

- SQLite Expert<br>
SQLite Expert is a powerful tool designed to simplify the development of SQLite3 databases. 

- Oculus<br>
Oculus is the most advanced all-in-one VR product.With the use of Oculus, AR can be better connected and immersive into the construction environment.

- RTAB-MAP<br>
Real-Time Appearance-Based Mapping can scan the construction environment to form a VR model

- Remote-RED<br>
With Remote-RED you can access your Node-RED Dashboard with your cell phone while you are underway. You can also send notifications from Node-RED.
 <br>

**Hardwares Setup**

Mobile Phone or Tablet with Lidar Camera<br>
Need to be able to run nodered and RTAB-MAP (if you want scan)
 <br>


## **Building Blocks**

The main blocks of the **Always in Sight**  platform are divided into 3 steps: 3D Scan on-site, user upload documentation, and VR remote monitoring.

- 3D Scan： the open-source RTAB-MAP as the scanning tool, and stores the scanning results in the database.
- Website development： Node-red and connects it with the database. Used to store user-uploaded files
- VR remote monitoring： Unity as the development tool. And transmit information between website database through MQTT。

**Main Steps**  
- 3D Scan &rarr; RTAB-MAP
- Website Development &rarr; Node-Red
- VR Development &rarr; Unity
<br>

![](./media/BBL_FlowChart.png)
 <br>

## **Reference Repo**

 <br>

**Unity Webviews**

In the early attempts, we explored some ways to put the dashboard link directly into the VR application, but later because the Node-red dashboard could not be used separately from Node-red, we gave up this method.

1. [Unity-Webview](https://github.com/gree/unity-webview)

2. [UnityOculusAndroidVRBrowser](https://github.com/IanPhilips/UnityOculusAndroidVRBrowser)    
 <br>

 **Login and Registration** <br> 

We first tried using html codes in node-red nodes to build html pages and use them, but then we found an easier way in github.<br>

1. [Login-and-Registration-using-Redis-in-Node-JS](https://github.com/Bugz296/Login-and-Registration-using-Redis-in-Node-JS)<br>

**File upload** <br>

We tried to use the uploaded node in Node-RED Dashboard, and found the method of inserting a html code, found and modified its issue to use this method.<br>

1. [File upload example](https://flows.nodered.org/flow/c70d0b4c54b583cf30c7e989b74feb68)<br>
2. [node-red-contrib-ui-upload](https://flows.nodered.org/node/node-red-contrib-ui-upload)<br>
3. [File upload and delete in Dashboard](https://flows.nodered.org/flow/5805a1141b1212d2c55c1781b9dbd15b)<br>
4. [File Upload using HTTP Request node](https://flows.nodered.org/flow/cbf44e064b9406a1175a7e8d589f66ac)<br>
5. [library node-red](https://flows.nodered.org/search?term=upload&type=flow)<br>

**File display** <br>

We tried to use the uploaded node in Node-RED Dashboard, and found the method of inserting a html code, found and modified its issue to use this method.<br>
1. [node-red-contrib-ui-media](https://flows.nodered.org/node/node-red-contrib-ui-media)<br>
2. [Node-Red Image on Dashboard](https://stackoverflow.com/questions/63077927/node-red-image-on-dashboard)<br>
3. [POST an image and display it in dashboard in Node-red](https://stackoverflow.com/questions/57316142/post-an-image-and-display-it-in-dashboard-in-node-red)<br>
4. [Using The Node-Red Dashboard Template](https://www.youtube.com/watch?v=J_-VCyz7kJE)<br>

## **Create User Interface**
**Approaches we tried**
1. Writing html codes in node-red nodes and connect it<br>
![](./media/home1.png) <br>
2. **Using Node-RED dashboard (final choice)**
![](./media/home3.png) <br>
![](./media/home4.png) <br>
![](./media/home5.png)<br>

<br>

**User log-in interface**
1. Html code<br>
![](./media/login2.png)<br>
![](./media/home1.png) <br>
![](./media/login1.png)<br>
2. **Repo from [github](https://github.com/Bugz296/Login-and-Registration-using-Redis-in-Node-JS) (final choice)**<br>
![](./media/login4.png)<br>
![](./media/login3.png)<br>
 <br>

**Upload interface**
1. Using [file upload node](https://flows.nodered.org/node/node-red-contrib-ui-upload) <br>
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
  ```npm install node-red-contrib-ui-upload```<br>
![](./media/upload.png)<br>

2. **Repo from [github](https://flows.nodered.org/flow/5805a1141b1212d2c55c1781b9dbd15b): Using template node to insert a html code on dashboard (final choice)**<br>

- issue <br>
The original code is based on Linux system and cannot be run on Windows system <br>
![](./media/file1.png)<br>
![](./media/file2.png)<br>
- how we modify it
![](./media/modify.png)<br>
![](./media/modify1.png)<br>
 <br>

**Select & display interface**

**Select**<br>

All the nodes we used to realize selecting function is from [dashboard](https://flows.nodered.org/node/node-red-dashboard)<br>
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
  ```npm install node-red-dashboard```

  ![](./media/project1.png) 

1. Select project -dropdown <br>
![](./media/project2.png) <br>
![](./media/project3.png) <br>
2. Select date - date<br>
![](./media/date2.png)<br>
![](./media/date3.png) <br>
3. Select zone - dropdown<br>
![](./media/zone2.png)<br>
![](./media/zone3.png)<br>

**Display**<br>
1.Using [media](https://flows.nodered.org/node/node-red-contrib-ui-media) node<br>
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
  ```npm install node-red-contrib-ui-media```<br>
![](./media/media1.png)<br>
![](./media/media2.png)<br>
2. Using [template](https://flows.nodered.org/node/node-red-dashboard) node<br>
The template node is included in the dashboard<br>
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
  ```npm install node-red-dashboard```<br>

![](./media/template1.png)<br>
![](./media/template2.png)<br>
![](./media/template3.png)<br>
 <br>

**Add marks on floor plan images**
 <br>
At first, we want to make this function to mark the place where we find problems. Once we find a problem in a specific point of a floor plan, we will add mark on this point, so that we can tell from the floor plan at this point it has some problems.

1. **Method 1**
- We try to mark coordinate points on the picture and record the position of the coordinate point. In the later interface, users could store the picture at this position for easy checking.
![](./media/Mark1_DB.jpg)
- We use Java script in Node-Red to make this function. But it's hard to save the coordinate as a button for users to check, so it undeveloped
![](./media/Mark1_NR.png)

2. **Method 2**
- We try to use add button then change the button coordinate to achieve add marks on images. And users could check the button with coordinate and save the 'problem' images on this coordinate.
![](./media/Mark2_DB.jpg)
- We use the context menu to add point on the image, then send information to database to save the coordinate. But it could not show again, if users change the image.
![](./media/Mark2_NR.png)

At last, we gave up on this function to mark because we decided to select a zone in a floor plan by numbering each zone in advance. In this way we can distinguish different zone. Once we find some problems in a specific point, we can select the zone number of this point and add problem descriptions and images to it.  
 <br>

## **Build up Database**

**Softwares**
1. MySQL 5.7 with Workbench 8
First create a connection
![mysql](./media/mysqlworkbench2.png)<br>
Then you can create a new table and add columns or directly write sql queries
![mysql](./media/mysqlworkbench4.png)<br>
The reason why we gave up on this software was we could not build up connection successfully between mysql and node-red.
<br>

2. DB Browser for SQLite
Next, we tried using sqlite to build up our database. This software was quite easy to create a table very fast. But there were a lot errors when we tried to set a primary key and foreign key, so we gave up on this software too.
![dbsqlite](./media/dbsqlite.png)
<br>

3. **SQLite Expert (final choice)**
Finally we chose sqlite expert as database built up software, because we could connect sqlite with node-red, and we could design database easily.
- Database design logic
![dbsqlite](./media/databasedesign.png)
<br>

- Tables
Users table
![sqlite](./media/table3.png)
![sqlite](./media/table6.png)<br>
Projects table
![sqlite](./media/table1.png)
![sqlite](./media/table4.png)<br>
Resources table
![sqlite](./media/table2.png)
![sqlite](./media/table5.png)

<br>

## **Connect Database with User Interface**
<br>

**How we connect database with Node-RED**
1. [mysql node](https://flows.nodered.org/node/node-red-node-mysql)
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
  ```npm i node-red-node-mysql```
<br>
Node properties:<br>
![node](./media/nodemysql3.png)<br>
Error:<br>
![node](./media/nodemysql2.png)<br>
Using this node, we could not connect mysql with node-red so we gave up.

<br>

2. [mysql2 node](https://flows.nodered.org/node/node-red-contrib-mysql2)
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
    ```npm i node-red-contrib-mysql2```
    <br>
Node properties:<br>
![node](./media/nodemysql21.png)<br>
Error:<br>
![node](./media/nodemysql23.png)<br>
Using this node, we could not connect mysql with node-red so we gave up.
<br>

3. [**sqlite node (final choice)**](https://flows.nodered.org/node/node-red-node-sqlite)
Either use the `Node-RED Menu` - `Manage Palette` - `Install`, or run the following command in your Node-RED user directory - `~/.node-red`:<br>
    ```npm i n--unsafe-perm node-red-node-sqlite```
    <br>
![node](./media/nodesqlite1.png)<br>
Node properties:<br>
![node](./media/nodesqlite2.png)
![node](./media/nodesqlite3.png)

<br>

**Store Registration Data to Database**

First, create registration flow like this:
![flow](./media/flowlogin.png)<br>
`Prepare SQL Params` node properties:
![flow](./media/flowlogin2.png)<br>
`SQL Statement` node properties:
![flow](./media/flowlogin3.png)<br>
`Set Notification Message` node properties:
![flow](./media/flowlogin4.png)<br>
`Shows Notification` node properties:
![flow](./media/flowlogin5.png)<br>
Then we can enter username and password, and the data will be saved in table `users`.
<br>

**Read User Log-in Data and Compare It with Database**

First, create log-in flow like this:
![flow](./media/flowlogin6.png)<br>
`Select` node properties:
![flow](./media/flowlogin7.png)
By this node we can select data from database.<br>

`Sqlite` node properties:
![flow](./media/flowupload11.png)<br>
`Decisions` node properties:
![flow](./media/flowlogin8.png)
By this node we can compare data from database with text that we enter to judge whether the password is right or not. If it is right, we can jump to the project tab and the user log-in information will be saved. If not, it will show a warning.<br>

`Save User & Jump` node properties:
![flow](./media/flowlogin9.png)<br>
`Jump to Project Tab` node properties:
![flow](./media/flowlogin10.png)<br>
`Shows Notification` node properties:
![flow](./media/flowlogin5.png)<br>
<br>

**Store Data to Database after Uploading a Project**

We split the uploading of files and the storage of data to the database into two steps. Above we have elaborate how we upload files to our storage, please check `Upload Interface` above. First we have to collect the text we enter, the filename of the image we upload and user information, and then we can save all of them by click button `SAVE`.
1. Create a flow like this to collect filename:
![flow](./media/flowupload1.png)<br>
`Get Filename` node properties:
![flow](./media/flowupload2.png)<br>
`Set Filename` node properties:
![flow](./media/flowupload3.png)
<br>By this node we can set the filename text to flow so that we can store later.<br>
`Display Project Image` node properties:
![flow](./media/flowupload4.png)<br>
`Initiate White Background` node properties:
![flow](./media/flowupload10.png)<br>
By this node everytime we jump to project upload tab, the area that display image will be clean and white. Without this node, the area will keep displaying the last image we upload, even if we refresh the page.
<br>

2. Create a flow like this to collect the project title text we enter: 
![flow](./media/flowupload5.png)<br>
`Initiate Text Output` node properties:
![flow](./media/flowupload12.png)<br>
By this node everytime we jump to project upload tab, the text input field will be blank. Without this node, the area will keep displaying the last project title text we enter, even if we refresh the page.<br>
`Project Title` node properties:
![flow](./media/flowupload7.png)<br>
`Set Projectname` node properties:
![flow](./media/flowupload6.png)
<br>By this node we can set the project title text to flow so that we can store later.
<br>

3. Create a flow like this to save all the data we collect: 
![flow](./media/flowupload8.png)<br>
`Insert` node properties:
![flow](./media/flowupload9.png)
<br>Above in `Read User Log-in Data and Compare It with Database` - `Save User & Jump node properties`, we have elaborated how we collect user_id data, please check. In this step we insert all the data we collect to table `projects`.<br>
`Sqlite` node properties:
![flow](./media/flowupload11.png)
At the end, we can store all the data to database.
<br>

**Read Data from Database, Make a Menu and Display Image**

First, create select menu flow like this:
![flow](./media/flowselect.png)<br>

`Select1` node properties:
![flow](./media/flowselect1.png)
<br>By this node we can select data from database.<br>

`Sqlite` node properties:
![flow](./media/flowupload11.png)<br>

`Set Options` node properties:
![flow](./media/flowselect2.png)
<br>By this node we can set each project title text we select from database as a option to payload <br>

`Project List` node properties:
![flow](./media/flowselect3.png)
<br>This node can receive message from the last node and list the options in a drop-down menu one by one. In this step the project select menu is created. <br>

`Select2` node properties:
![flow](./media/flowselect4.png)
<br>By this node,  after we click one option in the drop down menu we can select the exact project title which is corresponding to the option we select from database.<br>

`Set Project` node properties:
![flow](./media/flowselect5.png)
<br>By this node we can set the project title that we select in last step to flow. By this step we can collect the project_id information which we will use for view pictures of a specific problem.<br>

`Project Display Area` node properties
![flow](./media/flowselect6.png)
<br>By this node we can display the project floor plan image.
<br>

**Store Data to Database after Uploading a Photo in Resources Upload Interface**

This function is similar to the function `Store Data to Database after Uploading a Project` mentioned above. We split the uploading of files and the storage of data to the database into two steps. Above we have elaborate how we upload files to our storage, please check `Upload Interface` above. First we have to collect the text we enter, the date, the zone number where a problem happen, the description of this problem the filename of the image we upload and project title, and then we can save all of them by click button `SAVE`.
1. Create a flow like this to collect filename:
![flow](./media/flowupload13.png)<br>
`Get Filename` node properties:
![flow](./media/flowupload2.png)<br>
`Set Filename` node properties:
![flow](./media/flowupload14.png)
<br>By this node we can set the filename text to flow so that we can store later.<br>
`Display Resources Image` node properties:
![flow](./media/flowupload15.png)<br>
`Initiate White Background` node properties:
![flow](./media/flowupload10.png)
<br>By this node everytime we jump to project upload tab, the area that display image will be clean and white. Without this node, the area will keep displaying the last image we upload, even if we refresh the page.
<br>

2. Create a flow like this to collect the zone number where the problem happens: 
![flow](./media/flowupload16.png)<br>
`Initiate Text Output` node properties:
![flow](./media/flowupload12.png)
<br>By this node everytime we jump to project upload tab, the text input field will be blank. Without this node, the area will keep displaying the last zone number we choose, even if we refresh the page.<br>
`Select Zone Id` node properties:
![flow](./media/flowupload17.png)<br>
`Set Zone Id` node properties:
![flow](./media/flowupload18.png)
<br>By this node we can set the zone_id to flow so that we can store later.
<br>

3. Create a flow like this to collect the problem description text that we enter: 
![flow](./media/flowupload19.png)<br>
`Initiate Text Output` node properties:
![flow](./media/flowupload12.png)
<br>By this node everytime we jump to project upload tab, the text input field will be blank. Without this node, the area will keep displaying the last problem description text we enter, even if we refresh the page.<br>
`Enter Problem Description` node properties:
![flow](./media/flowupload20.png)<br>
`Set Problem Description` node properties:
![flow](./media/flowupload21.png)
<br>By this node we can set the problem description text to flow so that we can store later.
<br>

4. Create a flow like this to collect the project_id: 
![flow](./media/flowupload22.png)<br>
`Link in` node links from the end of flow `Read Data from Database, Make a Menu and Display Image` above. That means in this flow the drop-down menu `Select a Project` is sychronized with the one in flow `Read Data from Database, Make a Menu and Display Image`. Also, the `Select` node in this flow is the same with the `Select2` node in flow above, and `Sqlite` node and `Set Project` node in this flow are the same with the ones in flow above. Please check the properties in `Read Data from Database, Make a Menu and Display Image`.
<br>

5. Create a flow like this to collect the date data: 
![flow](./media/flowupload24.png)<br>
`Select Date` node properties:
![flow](./media/flowupload25.png)<br>
`Set Date` node properties:
![flow](./media/flowupload26.png)
<br>By this node we can set the date data to flow so that we can store later.
<br>

6. Create a flow like this to save all the data we collect: 
![flow](./media/flowupload28.png)<br>
`Insert` node properties:
![flow](./media/flowupload27.png)
<br>In this step we insert all the data we collect to table `resources`<br>
`Sqlite` node properties:
![flow](./media/flowupload11.png)
At the end, we can store all the data to database.
<br>

**View Pictures and Descriptions of a Specific Point and Date**

This function is similar to then function `Read Data from Database, Make a Menu and Display Image` mentioned above.

1. Create a flow like this to select the zone number: 
![flow](./media/flowselect7.png)
<br>All the nodes are the same with `Store Data to Database after Uploading a Photo in Resources Upload Interface` - `2. Create a flow like this to collect the zone number where the problem happens`
<br>

2. Create a flow like this to select the date: 
![flow](./media/flowselect8.png)<br>
`Select Date` node and `Set Date` node are the same with `Store Data to Database after Uploading a Photo in Resources Upload Interface` - `5. Create a flow like this to collect the date data` <br>
`Set Date Format` node properties:
![flow](./media/flowselect9.png)
<br>By this node we can keep the date picker initiated with the date of today. Without this node, the date picker will be initiated with the date Jan.1 1970, which will make us spend too much time on picking a right date.
<br>

3. Create a flow like this to view pictures and description of a specific zone and date:
![flow](./media/flowselect10.png)<br>
`Select` node properties:
![flow](./media/flowselect11.png)
<br>By this node we can select data from database.<br>
`Split` node properties:
![flow](./media/flowselect12.png)
<br>By this node we can split the filename array, which we get from database after selecting, so that each filename will be in a seperated line not together in a array.<br>
`Set Display Form for Each Payload` node properties:
![flow](./media/flowselect13.png)
<br>By this node we convert filename to the format that can display the corresponding image.<br>
`Join` node properties:
![flow](./media/flowselect14.png)
<br>By this node we join image display payload together.<br>
`Set Join Payload` node properties:
![flow](./media/flowselect15.png)
<br>By this node we set the joint image display payload to a integrated payload.<br>
`Resources Display Area` node properties:
![flow](./media/flowselect16.png)
<br>By this node we can display all the pictures we take in this zone.
At the end, we can view all the resources from time to time, zone to zone.
<br>

## Connect other applications with interface 
 <br>

**Create 3D Scanning Models on Mobile Devices**
 <br>

- We tried three apps to prototype the 3d scan module, **Fab-Scan**, **Canvas**, **RTAB-Map**.
- The Fab-Scan scan range is very limited, and the items to be scanned must be put into the box.
- Canvas is a developed application, but if users want to export the files they scand, they must payment each time.
- RTAB-Map is the application we choosed as our scan module, which is an oprn source 3D Scan, we could download the application from app store and also we could link it with our website.

 1. [Fab-Scan](https://github.com/mariolukas/FabScanPi-Server
)  
![](./media/FabScan.png)
 2. [Canvas](https://canvas.io/)
 ![](./media/Canvas.png)
 3. [RTAB-MAP](https://github.com/introlab/rtabmap/wiki/Tutorials)  
 ![](./media/RTAB_Map.png)

 <br>
 

**Convert 3D Scanning Models to VR Scene**

**Build Scene for Oculus**

- Link Oculus with computer
1. Open developer model on mobile phone application
 ![](./media/hacks_app-dev-mode.png)
2. Open Oculus Computer program, link with quest
 ![](./media/Link_QuestWithComputer.png)

- Link Oculus with Unity
1. Import VR Packages

   - Oculus XR Plugin
 ![](./media/IP_01.png)

   - Open XR Plugin
 ![](./media/IP_02.png)

   - XR Interaction Toolkit
 ![](./media/IP_03.png)

   - XR Plugin Management
 ![](./media/IP_04.png)

 2. Project Setting

    - Preset Manage
 ![](./media/IP_05.png)

    - XR Plug-in Management
 ![](./media/IP_06.png)

    - Oculus
 ![](./media/IP_07.png)

 3. Import Interaction to Hierarchy<br>
    ![](./media/IP_10.png)

 4. Creat Scene
    - Import Models<br>
   ![](./media/IP_11.png)

    - Load Scene<br>
   ![](./media/IP_12.png)

 
 5. Building Setting
    
    - Android<br>
      Click Build and Run, the apk file will automaticli run in Oculus Quest.
 ![](./media/IP_08.png)<br>
     If you could not find Android model in Build Setting, you should download it from Unity Hub.<br>
![](./media/IP_09.png)



**Connect VR With Website**

  1. Unity Program
     - Creat an empty objet and add the MQTT program to it.<br>
        ![](./media/IP_13.png)

  2. Node-red Program
     - Use MQTT to recive information from Unity<br>
        ![](./media/IP_14.png)

  3. Connect Unity with Node-red<br>
        ![](./media/IP_15.png)